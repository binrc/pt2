LDFLAGS="-L/usr/local/lib"

world:
	cc main.c -o pt

debug:
	gcc -ggdb main.c -o pt.debug && gdb pt.debug

run: world
	./pt

man: 
	mandoc pt.1 | less

clean: 
	rm ./pt
