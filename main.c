#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <signal.h>

// DATA DIVISION.
struct flag {
	char flag[8];
	char *value;
	char *description;
};

struct flag flags[7] = {
	{ "-i", "10", "Inhale time"},
	{ "-e", "20", "Exhale time"},
	{ "-h", "0", "Hold time"},
	{ "-c", "0", "Total cycles"},
	{ "-b", "1", "Enable beep"},
	{ "--debug", "", "Print debug info"},
	{ "--help", "", "Print this text"}
};

char *cols[] = {"\033[32m", "\033[33m", "\033[31m"};

int performedCycles = 0;	// >
unsigned long long ttime = 0;	// > needs to be global for the signal handler
int dlev = 0;			// > 

// debugging 
void dumpvars(){
	int i;

	printf("DEBUG INFO: \n");
	
	for(i = 0; i<sizeof(flags)/sizeof(flags[0]); i++){
		printf("flag: %s\tvalue: %s\n", flags[i].flag, flags[i].value);
	}
	printf("Cycle counter: %d\n", performedCycles);
	printf("Total runtime: %llu\n", ttime);

}

// results printer
void results(){
	printf("\n%d cycles: \ttotal time: %llu\n", performedCycles, ttime);
}

// C-c handler
void sigintHandler(int signum){
	signal(SIGINT, sigintHandler);
	if(dlev > 0)
		dumpvars();

	results();
	exit(0);
}

// help text
void help(){
	int i;

	printf("Usage:\n");

	for(i = 0; i<sizeof(flags)/sizeof(flags[0]); i++){
		if(!strcmp(flags[i].value, "")){
			printf("%s\t%s\n", flags[i].flag, flags[i].description);
		} else {
			printf("%s\t%s\t\tdefault: %s\n", flags[i].flag, flags[i].description, flags[i].value);
		}
	}

	exit(0);
};


// print timer 
unsigned long long printer(char *text, int dat1, int dat2, unsigned long long ttime, int bell, char *col){
	printf("\r                          \r");
	printf("%s%s: %d/%d\033[0m", col, text, dat1, dat2);
	if(bell)
		printf("\a");
	fflush(stdout); // required to prevent vt from buffering stdout at \n
	ttime++;
	sleep(1);
	return ttime;
}

// main logic
int timer(){
	int itime	= atoi(flags[0].value);
	int etime	= atoi(flags[1].value);
	int htime	= atoi(flags[2].value);
	int cycles	= atoi(flags[3].value);
	char bell	= atoi(flags[4].value);
	int t = 0;

	signal(SIGINT, sigintHandler); 	// add C-c handler

	if(cycles == 0)
		cycles = INT_MAX;

	while(performedCycles < cycles){

		for(t = 1; t<=itime; t++)
			ttime = printer("Inhale", t, itime, ttime, bell, cols[0]);
		
		for(t = 1; t<= htime; t++)
			ttime = printer("Hold", t, htime, ttime, bell, cols[1]);

		for(t = 1; t<= etime; t++)
			ttime = printer("Exhale", t, etime, ttime, bell, cols[2]);
	
		performedCycles++;
	}

	return 0;
}

int main(int argc, char *argv[]){
	int i, j;

	// start at 1, argv[0] = cmd that spawned this proc
	for(i = 1; i < argc; i ++){
		if(!strcmp(argv[i], "--help"))
			help();

		if(!strcmp(argv[i], "--debug")){
			flags[5].value = "1";
			dlev = 1;
			continue;
		}


		for(j = 0; j < sizeof(flags)/sizeof(flags[0]); j++){
			if(!strcmp(argv[i], flags[j].flag)){
				flags[j].value = argv[i+1];
				i++;
				break;
			}
		}


	}

	timer();

	results();

	if(dlev > 0)
		dumpvars();
	return 0;
};
