# Description

C re-write of the meditation timer

# Usage

```
-i	Inhale time		default: 10
-e	Exhale time		default: 20
-h	Hold time		default: 0
-c	Total cycles		default: 0
-b	Enable beep		default: 1
--debug	Print debug info
--help	Print this text
```

# TODO

- [x] build core prog
- ~[ ] sndio support~
- ~[ ] pluseauido support~
- [X] use the ternimal bell like a sane person
- [X] add more ANSI magic
